# COMP1001 2nd coursework
# GCD algorithm

		.data
prmpt1:		.asciiz "Input first value: "
prmpt2:		.asciiz "Input second value: "
str1:		.asciiz "The greatest common divisor ist "
gcd_result:     .word 0
		.text
		.globl init

init:
	li $v0, 4				# set system call code to print string
	la $a0, prmpt1				# load address pf prmpt1 to $a0
	syscall					# print out prmpt1
	li $v0, 5				# set system call to read int
	syscall
	
	move $a1, $v0				# move value from $v0 to $a0
	
	li $v0, 4				# set system call code to print string
	la $a0, prmpt2				# load address pf prmpt1 to $a0
	syscall					# print out prmpt1
	li $v0, 5				# set system call to read int
	syscall
	
	move $a2, $v0				# move value from $v0 to $a2
	
	abs $a1, $a1				# make u a positive number
	abs $a2, $a2				# make v a positive number
	
	jal gcd					# jump and link to gcd algorithm
	sw $v0, gcd_result			# store result
	j terminate				# jump to exit
		
gcd:
	# create stack memory
	addiu $sp, $sp, -12			# create memory space in stack
	sw $fp, 0($sp)				# load frame pointer address
	sw $ra, 4($sp)				# load return address
	addiu $fp, $sp, 8			# update stack memory location
	
	# termination cases
	add $v0, $a1, $0
	beq $a1, $a2, stackpop			# u == v
	add $v0, $a1, $0
	beqz $a1, stackpop			# u == 0
	add $v0, $a2, $0
	beqz $a2, stackpop			# v == 0, there is an exception here make on later
	
	# check if numbers are odd or even
	andi $v0, $a1, 0x1			# check if yu is odd/even
	bnez $v0, u_odd				# if u is odd -> branch
	
	andi $v0, $a2, 0x1			# check if v is odd/even
	beqz, $v0, uv_even 			# u and v are eve -> branch
	
	# u is even v is odd (case)
	srl $a1, $a1, 0x1			# divide u by 2
	j recursion				# jump

u_odd:
	andi $v0, $a2, 0x1			# check if v is odd/even
	bnez, $v0, uv_odd			# u and v are eve -> branch
	srl $a2, $a2, 0x1			# divide v by 2
	j recursion				# jump
uv_odd:
	add $t1, $a1, $0			# copy $a1 to $t1
	add $t2, $a2, $0			# copy $a2 to $t2
	sub $t3, $t2, $t1			# v - u (temporary storage) store in $t3
	srl $t3, $t3, 0x1			# divide $t3 by 2
	add $a1, $t3, $0			# copy $t3 to $a1
	add $a2, $t2, $0			# copy $t2 to $a2
	
	slt $t4, $t1, $t2			# check if u is less than v
	beqz $t4, recursion			# u is less then v -> branch
	
	# this process assumes u is greater or equal to v -> since the check failed to branch
	sub $t3, $t1, $t2			# u - v (temporary storage) store in $t3
	srl $t3, $t3, 0x1			# divide $t3 by 2
	add $a1, $t3, $0			# copy $t3 to $a1
	j recursion
uv_even:
	li $t5, 0x1				# make multiplier
	sw $t5, 8($sp)				# store multiplier in stack memory
	srl $a1, $a1, 0x1			# divide u by 2
	srl $a2, $a2, 0x1			# divide v by 2
recursion:
	jal gcd
	lw $t5, 8($sp)				# load multiplier from memory stack
	beqz $t5, stackpop			# check if multiplier is 0
	sllv $v0, $v0, $t5			# multiply result by 2
stackpop:
	lw $fp, 0($sp)				# load frame pointer
	lw $ra, 4($sp)				# load return address
	addiu $sp, $sp, 12			# update frame pointer
return:
	jr $ra					# register jump to previous address location linked to the register
	
terminate:
	li $v0, 4				# set system call code to print string
	la $a0, str1				# load address pf prmpt1 to $a0
	syscall					# print out prmpt1
	li $v0, 1				# set system call to read int
	lw $a0, gcd_result
	syscall
	
	li $v0, 10
	syscall
